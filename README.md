Hola! Soy Santiago Bruno.
=========================

Los pasos a seguir son los siguientes:
--------------------------------------

$git clone https://gitlab.com/santt31/Ansible_AWS.git entregable_aws && cd entregable_aws 
$vagrant up 																#Levantará las dos maquina el centos y devel
$vagrant ssh centos
$ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts_devel   #Provisiono VM devel
$ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts_aws		#Provisiono a mi aws
$exit

VERIFICACIONES:
---------------


														### VM DEVEL
														-----------
$vagrant ssh devel
$cat ~/.bashrc
(se podra ver y tambien ver la apariencia del entorno color verde-azul)
--------
$cat /etc/php.ini
(se podran ver los valores como display_errors,error_reporting,max_execution_time, date.timezone),
seteados correctamente
----
$composer --version
$exit


														### VM AWS
														-----------
$ssh -l root -i keys/prod_provision_key.pem 52.17.183.213
$cat ~/.bashrc
(se podra ver y tambien ver la apariencia del entorno color rojo)
--------
$cat /etc/php.ini
(se podran ver los valores como display_errors,error_reporting,max_execution_time, date.timezone),
seteados correctamente
---
$composer --version
$exit

											###COMUNES(VM DEVEL Y AWS LO TIENEN )
											------------------------------------
$htop
$wget --version
$git --version
$tree --version
$php --version
$composer --version







